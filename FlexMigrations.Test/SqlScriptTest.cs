﻿using FlexMigrations.Implementation;
using NUnit.Framework;

namespace FlexMigrations.Test
{
  [TestFixture]
  public class SqlScriptTest
  {
    [Test]
    public void Null()
    {
      SqlScript script = new SqlScript(null);
      Validate(null, null, -1, script);
    }

    [Test]
    public void Empty()
    {
      SqlScript script = new SqlScript("");
      Validate("", "", -1, script);
    }

    [Test]
    public void WithoutDots()
    {
      const string fullName = "01sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, -1, script);
    }

    [Test]
    public void NotSql()
    {
      const string fullName = "01.txt";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, -1, script);
    }

    [Test]
    public void EmptySql()
    {
      const string fullName = ".sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, -1, script);
    }

    [Test]
    public void WithoutIndex()
    {
      const string fullName = "upgrade01.sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, -1, script);
    }

    [Test]
    public void OnlyDots()
    {
      const string fullName = "..sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, -1, script);
    }

    [Test]
    public void NegativeIndex()
    {
      const string fullName = "-10.sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, -1, script);
    }

    [Test]
    public void OnlyIndex()
    {
      const string fullName = "010.sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, 10, script);
    }

    [Test]
    public void BigIndex()
    {
      const string fullName = "910001.sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, 910001, script);
    }

    [Test]
    public void NotOnlyIndex()
    {
      const string fullName = "010_desc.sql";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, 10, script);
    }

    [Test]
    public void OnlyIndexSeveralDots()
    {
      const string namespaceStr = "a.n1.n2";
      const string scriptName = "010.sql";
      string fullName = $"{namespaceStr}.{scriptName}";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, scriptName, 10, script);
    }

    [Test]
    public void OnlyIndexUppercase()
    {
      const string fullName = "010.SQL";
      SqlScript script = new SqlScript(fullName);
      Validate(fullName, fullName, 10, script);
    }

    private void Validate(string expectedFullName, string expectedShortName, int expectedIdx, SqlScript script)
    {
      Assert.AreEqual(expectedFullName, script.FullName);
      Assert.AreEqual(expectedShortName, script.ShortName);
      Assert.AreEqual(expectedIdx, script.Index);
    }
  }
}
