﻿using System;
using Microsoft.Extensions.Logging;

namespace PostgreSqlMigrationSample
{
  public static class Program
  {
    public static void Main()
    {
      const string connectionStr = "Host=192.168.142.229;Port=5432;Username=postgres;Password=iskratel;Database=ehm-dlym";

      var loggerFactory = new LoggerFactory();
      loggerFactory.AddLog4Net();

      MigrationFacade facade = new MigrationFacade();
      facade.Run(loggerFactory, connectionStr);

      Console.WriteLine("Finished. Press Enter to exit");
      Console.ReadLine();
    }
  }
}
