﻿using System;
using System.Data.SQLite;
using FlexMigrations;
using Microsoft.Extensions.Logging;

namespace SqliteMigrationSample
{
  public static class MigrationsFacade
  {
    public static void Run(ILoggerFactory loggerFactory, string connectionStr)
    {
      try
      {
        IDbConnectionFactory connectionFactory = new DbConnectionFactory<SQLiteConnection>(connectionStr);
        MigrationManager migrationManager = new MigrationManager(loggerFactory);
        migrationManager.RunMigrations(connectionFactory, ErrorPolicy.AllOrNothing, false);
      }
      catch (Exception e)
      {
        Console.WriteLine("Failed to migrate DB: " + e.Message);
      }
    }
  }
}
