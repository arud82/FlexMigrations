﻿using System;
using Microsoft.Extensions.Logging;

namespace SqliteMigrationSample
{
  public static class Program
  {
    public static void Main()
    {
      const string connectionStr = "Data Source=../../sqlite.db";

      var loggerFactory = new LoggerFactory();
      loggerFactory.AddLog4Net();

      MigrationsFacade.Run(loggerFactory, connectionStr);

      Console.WriteLine("Finished. Press Enter to exit");
      Console.ReadLine();
    }
  }
}
