﻿using System;
using System.Data.SqlClient;
using FlexMigrations;
using log4net;
using Microsoft.Extensions.Logging;

namespace SqlServerMigrationSample
{
  public static class MigrationsFacade
  {
    private static readonly ILog Log = LogManager.GetLogger(nameof(MigrationsFacade));

    public static void Run(ILoggerFactory loggerFactory, string connectionStr)
    {
      try
      {
        IDbConnectionFactory connectionFactory = new DbConnectionFactory<SqlConnection>(connectionStr);
        MigrationManager migrationManager = new MigrationManager(loggerFactory);
        migrationManager.RunMigrations(connectionFactory, ErrorPolicy.AllOrNothing, true);
      }
      catch (Exception e)
      {
        Log.Error("Failed to migrate DB: " + e.Message);
      }
    }
  }
}
