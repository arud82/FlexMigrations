﻿using System;
using Microsoft.Extensions.Logging;

namespace SqlServerMigrationSample
{
  public static class Program
  {
    public static void Main()
    {
      const string connectionStr = "Data Source=localhost;Initial Catalog=FLEX_TEST;User ID=sa;Password=123;";

      var loggerFactory = new LoggerFactory();
      loggerFactory.AddLog4Net();

      MigrationsFacade.Run(loggerFactory, connectionStr);

      Console.WriteLine("Finished. Press Enter to exit");
      Console.ReadLine();
    }
  }
}
