﻿// The implementation is much nicer than SqlServerMigrator, because allows to execute script file as a whole, without splitting it by GO statements.
// Tut it requires Microsoft's SMO library (nuget Microsoft.SqlServer.SqlManagementObjects), which unfortunately has problems with BatchParser
// https://connect.microsoft.com/SQLServer/feedback/details/3140836/smo-nuget-packages-do-not-work-reliably-due-to-a-gac-dependency-on-sqlserver-batchparser-dll
// So, the implementation is commented out until appropriate fix.

/*
using System;
using System.Data.SqlClient;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;

namespace FlexMigrations.Implementation
{
  internal class SqlServerSmoMigrator: Migrator
  {
    private readonly SqlConnection _con;
    private readonly Server _sqlServer;
    private readonly ServerConnection _srvCon;

    public SqlServerSmoMigrator(string connectionStr, IVersionTableScripts versionTableScripts) : base(versionTableScripts ?? new VersionTableSqlServerScripts())
    {
      _con = new SqlConnection(connectionStr);
      _srvCon = new ServerConnection(_con);
      _sqlServer = new Server(_srvCon);
    }

    protected override void ExecuteScript(string script)
    {
      _sqlServer.ConnectionContext.ExecuteNonQuery(script);
    }

    protected override T ExecuteQuery<T>(string query)
    {
      return (T)_sqlServer.ConnectionContext.ExecuteScalar(query);
    }

    protected override void ExecuteInTransaction(Action action)
    {
      try
      {
        _srvCon.BeginTransaction();
        action();
        _srvCon.CommitTransaction();
      }
      catch
      {
        _srvCon.RollBackTransaction();
        throw;
      }
    }

    protected override bool LockDb()
    {
      try
      {
        ExecuteScript($"ALTER DATABASE {_con.Database} SET RESTRICTED_USER WITH ROLLBACK IMMEDIATE");
        return true;
      }
      catch (Exception e)
      {
        Log.Error("Failed to lock DB", e);
        return false;
      }
    }

    protected override void UnlockDb()
    {
      ExecuteScript($"ALTER DATABASE {_con.Database} SET MULTI_USER");
    }

    public override void Dispose()
    {
      _con.Dispose();
    }
  }
}
*/