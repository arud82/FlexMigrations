﻿using System.Collections.Generic;

namespace FlexMigrations.Implementation
{
  public abstract class VersionTableScriptsBase: IVersionTableScripts
  {
    protected const string TableName = "db_version";
    protected const string Version = "version";
    protected const string ScriptName = "script_name";
    protected const string DateName = "date";

    public abstract List<string> CreateScripts { get; }
    public abstract string CheckExistence { get; }
    public abstract string GetDbVersion { get; }
    public abstract string SetDbVersion(int version, string scriptName);
    public abstract bool ProcessCheckExistenceRes(object sqlRes);
    public abstract int ProcessGetDbVersionRes(object sqlRes);
  }
}
