﻿using System;
using System.Collections.Generic;

namespace FlexMigrations.Implementation.PostgreSql
{
  internal class PostgreSqlMigrator: Migrator
  {
    protected override IVersionTableScripts GetVersionTableScripts() => new VersionTablePostgreSqlScripts();

    protected override void ExecuteScript(string script)
    {
      IEnumerable<string> statements = new List<string>(){script};
      ExecuteNonQueries(statements);
    }

    protected override bool LockDb()
    {
      throw new NotImplementedException("DB locking is not supported for PostrgreSQL");
    }

    protected override void UnlockDb()
    {
      throw new NotImplementedException("DB locking is not supported for PostrgreSQL");
    }
  }
}
